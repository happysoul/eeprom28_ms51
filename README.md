## 工程项目地址  
https://oshwhub.com/firseve/eeprom28_ms51  

## exe可执行文件下载  
https://gitee.com/happysoul/eeprom28_ms51/releases  

## 基于 MS51FB9AE 的 EEPROM 烧录器  
用于读写 AT28C64B 和 AT28C256  
只读 27C64 27C128 27C256 27C512 石英窗口紫外线擦除芯片  
只读 W27E257 W27C512 华邦电擦除EEPROM  

擦除功能仅适用 AT28C256 全片写入 0xFF  
AT28C64B 可以写入，擦除功能不知原因不好用  

**2023-03-30**  
修改 AT28C64 兼容，延长写入后等待时间(数据写入后需要等待芯片内部把数据稳定写入后才能写下一个地址)  
增加 AT28C17 和 AT28C16 读写功能

没有用12v升压电路，无法擦写27芯片(只读)  

新唐MS51FB9AE 出厂没有ISP程序，需要使用NULINK下载  
或者烧录4k LDROM并配置LDROM启动(bin目录下MS51_UART0_ISP_LDROM.bin)  
可以考虑使用 N76E003 工程配置自己修改，修改内置RC频率  
未知问题是 N76E003 的 SPI 功能如果不能使用就配置普通IO方式控制595  
对应的速度会慢很多  

重新编译新唐的 ISP下载工具 bin目录下 NuvoISP.exe  
解决读取芯片后只能下载使用1/2剩余rom的软件bug(新唐工程师说这是ISP软件问题)  

## 目录结构：  
bin ISP工具和LDROM  
keil 单片机程序源码  
tools 用于生成测试用的bin文件  
main.py PC客户端主程序  
eeprom.py 串口通信依赖文件  
main_ui.ui pyQT5软件界面布局图  
main_ui.py pyQT5转成py可以调用的文件  

xp目录是在 winxp 下开发和使用的版本，未完全测试

## 其他备注  
ms51 默认没有LDROM，无法使用 ISP 工具下载，只能用 NULINK 写入 或者买芯片的时候让商家写入LDROM  
win7sp1 可能缺少vc运行库，需要手动安装 https://gitee.com/happysoul/eeprom28_ms51/releases/download/1.0/VC_x86.zip  


VC依赖关系查看地址  
https://wiki.python.org/moin/WindowsCompilers

## 更新程序  
如果烧录了 LDROM，可以通过 NuvoISP.exe 工具写入bin   
使用方法：  
1. usb连接电脑,打开 bin/NuvoISP.exe 选择 UART 和 串口，点击 connect
2. 按一下板子上的 reset 按钮，软件提示文字变绿，提示连接成功
3. 选择 APROM 对应工程中的 keil/KEIL/Output/Project.bin  
4. 勾选Programming 中 APROM 和 Reset and run
5. 点击软件中 start 下载程序到芯片
6. 再按一次板子的 reset 按钮就可以使用了  
参考图  
![Image text](https://gitee.com/happysoul/eeprom28_ms51/raw/master/bin/ISP_UPDATE.png)

软件界面  
![Image text](https://gitee.com/happysoul/eeprom28_ms51/raw/master/main.png)
