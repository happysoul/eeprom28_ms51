#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import struct

file16    = "2k-ff-16.bin"
file16_00 = "2k-00-16.bin"
file16_0F = "2k-0f-16.bin"
file64    = "8k-ff-64.bin"
file64_0F = "8k-0f-64.bin"
file128 = "16k-ff-128.bin"
file256 = "32k-ff-256.bin"
file256_0F = "32k-0f-256.bin"
file512 = "64k-ff-512.bin"

# 2k  2**11
# 8k  2**13
# 32k 2**15
# 64k 2**16

with open(file16, mode='wb+') as f:
    for i in range(2 ** 11):
        s = struct.pack('B', 0xff)
        f.write(s)
with open(file16_00, mode='wb+') as f:
    for i in range(2 ** 11):
        s = struct.pack('B', 0)
        f.write(s)
with open(file16_0F, mode='wb+') as f:
    for i in range(2 ** 11):
        if i <= 0x7ff:
            s = struct.pack('B', 0xff&i)
        else:
            s = struct.pack('B', 0xff)
        f.write(s)

with open(file64, mode='wb+') as f:
    for i in range(2 ** 13):
        s = struct.pack('B', 0xff)
        f.write(s)

with open(file64_0F, mode='wb+') as f:
    for i in range(2 ** 13):
        if i<0x1fff:
            s = struct.pack('B', i & 0xff)
        else:
            s = struct.pack('B', 0xff)
        f.write(s)

with open(file128, mode='wb+') as f:
    for i in range(2 ** 14):
        s = struct.pack('B', 0xff)
        f.write(s)

with open(file256, mode='wb+') as f:
    count = 0
    for i in range(2 ** 15):
        s = struct.pack('B', 0xff)
        f.write(s)

with open(file256_0F, mode='wb+') as f:
    count = 0
    for i in range(2 ** 15):
        if count<=0x1fff:
            s = struct.pack('B', count & 0xff)
            count += 1
        else:
            s = struct.pack('B', 0xff)
        f.write(s)

with open(file512, mode='wb+') as f:
    count = 0
    for i in range(2 ** 16):
        if count<=0xff:
            s = struct.pack('B', count & 0xff)
            count += 1
        else:
            s = struct.pack('B', 0xff)
        f.write(s)