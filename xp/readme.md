# winxp

功能是在winxp下使用方法


## 编辑器

历史版本 xp支持 2018.3.7

https://www.jetbrains.com/pycharm/download/other.html


## 低版本xp可用py

https://www.python.org/ftp/python/3.4.4/python-3.4.4.msi


## 切换源（清华、阿里）

pip config set global.index-url https://mirrors.aliyun.com/pypi/simple/

pip config set global.index-url https://pypi.tuna.tsinghua.edu.cn/simple/

pip install pyserial==3.0

pip install pypiwin32==219

pip install pyinstaller==3.2.1


## pyinstaller 需要UPX，解压upx.exe到pyinstaller 同级目录中（使用upx压缩后，制作的exe可能无法运行）

https://github.com/upx/upx/releases/

https://github.com/upx/upx/releases/download/v3.96/upx-3.96-win32.zip


## 打包脚本 生成 exe 文件

pyserial 高版本无法运行，只能用 3.0

pyinstaller -w -F eepromgui.py


# 使用QT4制作xp支持的客户端

下载QT4

https://download.lfd.uci.edu/pythonlibs/archived/cp34/PyQt4-4.11.4-cp34-cp34m-win32.whl

使用 pip 安装 

pip install PyQt4-4.11.4-cp34-cp34m-win32.whl

打包制作

pyinstaller -w -F main.py




